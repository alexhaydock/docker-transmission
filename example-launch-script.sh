#!/bin/sh

# Set ownership correctly
sudo chown -R 5555:5555 "/path/to/transmission/config"

# Start Transmission
docker-compose up -d --force-recreate