[![](https://images.microbadger.com/badges/image/alexhaydock/transmission-daemon.svg)](https://hub.docker.com/r/alexhaydock/transmission-daemon "Badge")

### How to Use
```sh
docker-compose up -d --force-recreate
```
